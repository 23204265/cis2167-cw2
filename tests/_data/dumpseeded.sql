-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: CW2
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.17.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2018_03_28_145347_create_questionnaires_table',1),('2018_03_28_145411_create_questions_table',1),('2018_03_28_145418_create_answers_table',1),('2018_03_28_145437_create_roles_table',1),('2018_03_28_145447_create_permissions_table',1),('2018_03_28_145501_create_role_user_table',1),('2018_03_28_145512_create_permission_role_table',1),('2018_03_28_145542_create_question_questionnaire_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_questionnaire`
--

DROP TABLE IF EXISTS `question_questionnaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_questionnaire` (
  `question_id` int(11) NOT NULL,
  `questionnaire_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_questionnaire`
--

LOCK TABLES `question_questionnaire` WRITE;
/*!40000 ALTER TABLE `question_questionnaire` DISABLE KEYS */;
/*!40000 ALTER TABLE `question_questionnaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaires`
--

DROP TABLE IF EXISTS `questionnaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaires` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `published_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `questionnaires_id_unique` (`id`),
  UNIQUE KEY `questionnaires_title_unique` (`title`),
  KEY `questionnaires_published_at_index` (`published_at`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaires`
--

LOCK TABLES `questionnaires` WRITE;
/*!40000 ALTER TABLE `questionnaires` DISABLE KEYS */;
INSERT INTO `questionnaires` VALUES (1,'Enim aut nobis molestiae corrupti quisquam.','Possimus similique voluptas iusto dignissimos possimus nemo eum. Nulla quis debitis totam ut iste. Et soluta atque debitis sapiente sunt ipsa porro occaecati. Repellat voluptatem eius aut praesentium autem.','suscipit',1,'2018-04-21 14:44:12','2018-04-21 14:44:12','0000-00-00 00:00:00'),(2,'Aut est ducimus ut ipsam ut qui.','Voluptatem sint velit qui iusto nihil. Doloribus temporibus quibusdam qui qui. Porro totam dolorem culpa perspiciatis impedit qui aliquid. Aspernatur aspernatur quia soluta dolor.','aut',1,'2018-04-21 14:44:12','2018-04-21 14:44:12','0000-00-00 00:00:00'),(3,'A nulla dolores blanditiis nulla ut dignissimos.','Dolore in dicta commodi quod corporis. Ratione voluptatibus ullam et. Expedita nemo aliquam quia iure.','eos',1,'2018-04-21 14:44:12','2018-04-21 14:44:12','0000-00-00 00:00:00'),(4,'Rerum et minus laboriosam officiis ullam saepe.','Qui quibusdam qui debitis tempora et rerum. Placeat neque occaecati suscipit sunt. Nihil id assumenda et ratione.','deserunt',1,'2018-04-21 14:44:12','2018-04-21 14:44:12','0000-00-00 00:00:00'),(5,'Excepturi laborum quos quaerat aperiam.','Velit delectus ut distinctio nulla rerum. Aliquid facilis fugit in corrupti est neque eos. Quam ad odio vero.','laudantium',1,'2018-04-21 14:44:12','2018-04-21 14:44:12','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `questionnaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `questions_id_unique` (`id`),
  UNIQUE KEY `questions_title_unique` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (1,'What is your favourite colour?','select colour',NULL,NULL),(2,'what is your favourite animal?','choose animal',NULL,NULL);
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(320) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'XXXX','frankie@example.com','$2y$10$eH.sYNxFYTN5g6Csn41IROBA1','sLoMlxkPlk',NULL,NULL),(2,'Bobby Larkin','fabian.schoen@example.com','$2y$10$CFC49qXen9aCP1MtQtp6eenXV','ehOv8LizKX','2018-04-21 14:44:11','2018-04-21 14:44:11'),(3,'Mr. Jared Spencer IV','taurean49@example.net','$2y$10$AWzRJTGdU7tj/aaJI5W5P.S8v','fbOygSlyyT','2018-04-21 14:44:11','2018-04-21 14:44:11'),(4,'Dr. Kip Bruen','finn92@example.org','$2y$10$C2aewWD2tx1oBXYlA1nmcO2XQ','TTBifkqx7m','2018-04-21 14:44:11','2018-04-21 14:44:11'),(5,'Colten Reilly','yasmeen31@example.com','$2y$10$dTrLygdNQ.JD.TurshpWn.rYE','jOUtYSwBDI','2018-04-21 14:44:11','2018-04-21 14:44:11'),(6,'Maud Bosco','keshawn22@example.org','$2y$10$Aj9TReOWmN/mfYEPPIf7s.6uq','GCoY1afFfS','2018-04-21 14:44:11','2018-04-21 14:44:11'),(7,'Bert Donnelly','garfield64@example.org','$2y$10$3xv/oDmrNvskfpNbK.Nzw.y/D','qqz9NUHJz4','2018-04-21 14:44:11','2018-04-21 14:44:11'),(8,'Prof. Annette Hamill Sr.','toy.hudson@example.net','$2y$10$DZFU89HOoHWG5YTSiUaPZeaER','3cctJX9ykr','2018-04-21 14:44:11','2018-04-21 14:44:11'),(9,'Mireya Funk','guadalupe.rau@example.org','$2y$10$X4szE6E9fIta9.C/x8Y5l.u42','UyKXWIpxWR','2018-04-21 14:44:11','2018-04-21 14:44:11'),(10,'Kirsten Bins','dustin05@example.org','$2y$10$1hc4P6oDqNnsQ/Qe.a9hC.lQr','wXgfj0qOt4','2018-04-21 14:44:11','2018-04-21 14:44:11'),(11,'Jason Kris','ed96@example.com','$2y$10$625zBAi/4RgZr2/w.6Zv0ON/y','91drPOLJZJ','2018-04-21 14:44:11','2018-04-21 14:44:11'),(12,'Larry Schultz','block.marguerite@example.com','$2y$10$W8jeZMqEmu4ijueUSP5cpOKp/','nNHdVgCLel','2018-04-21 14:44:11','2018-04-21 14:44:11'),(13,'Dr. Jonathan Metz','oankunding@example.org','$2y$10$getkDWSOxMugW9tBiLm42OMS3','3UTui70nwE','2018-04-21 14:44:11','2018-04-21 14:44:11'),(14,'Mrs. Shanny O\'Keefe PhD','walter.judy@example.com','$2y$10$r7mJVeTLgrArUs3GhQ95oOjcd','GzEXlgQFjR','2018-04-21 14:44:11','2018-04-21 14:44:11'),(15,'Mr. Hilton King','kieran97@example.net','$2y$10$DFyQrvDgueuFgNlBXznkK.YCq','EeDkzdv19O','2018-04-21 14:44:11','2018-04-21 14:44:11'),(16,'Prof. Kade Bradtke','egutkowski@example.org','$2y$10$6iETdncmiTz1eMzOb4N4FeRou','wI3lTzjliT','2018-04-21 14:44:11','2018-04-21 14:44:11'),(17,'Regan Jast','nona07@example.com','$2y$10$RD.eDcxahmlzrgw2Oga/7eZnj','emjGVGQ16q','2018-04-21 14:44:11','2018-04-21 14:44:11'),(18,'Jedidiah Robel','elouise84@example.org','$2y$10$hC1Qz8GAjv9JoD.IL8xsV.uCt','7lKKt0gxwu','2018-04-21 14:44:11','2018-04-21 14:44:11'),(19,'Jodie Lang IV','mohammed.kovacek@example.com','$2y$10$MOGqp8JUGIu4jMcInZDksuXEL','uWwwNv8lsl','2018-04-21 14:44:11','2018-04-21 14:44:11'),(20,'Mr. Craig Ratke','odickens@example.com','$2y$10$AuNOHwdJ.mynlzgTzWqyQe0Bf','u2zEiYliuW','2018-04-21 14:44:11','2018-04-21 14:44:11'),(21,'Dejon Boehm','king10@example.org','$2y$10$nCs38r/DKwGneFW9Z0ruzOPRE','T49kw239CI','2018-04-21 14:44:11','2018-04-21 14:44:11'),(22,'Nakia Pollich','joana35@example.org','$2y$10$Ymt3MyxC6g9BaLfXjCfzDO2.a','wZCLUftYVb','2018-04-21 14:44:11','2018-04-21 14:44:11'),(23,'Gerard Fahey','chanelle.goodwin@example.com','$2y$10$cm2GJiIiJt4w/120XcZtSOlY7','qqm5vl07Xd','2018-04-21 14:44:11','2018-04-21 14:44:11'),(24,'Nadia Shields III','laury39@example.org','$2y$10$27T6yb6gzQwNGjuG.rOvYuPis','K053vOfsZn','2018-04-21 14:44:11','2018-04-21 14:44:11'),(25,'Dariana Turcotte','uhuel@example.org','$2y$10$wPtUiycRJTgyGNHQrtefoubn0','hi9mgl0rk0','2018-04-21 14:44:11','2018-04-21 14:44:11'),(26,'Prof. Juvenal Schaden DVM','qveum@example.org','$2y$10$blktoL04kBOXtTxNLHQXJubiY','Gbqbs3Wsd4','2018-04-21 14:44:11','2018-04-21 14:44:11'),(27,'Kenna Rempel','qhackett@example.org','$2y$10$loZs4zExTMrFwPQKPbQFnOGI/','FP14pJHGhy','2018-04-21 14:44:11','2018-04-21 14:44:11'),(28,'Johanna Konopelski I','bode.jedidiah@example.net','$2y$10$X7Kr.b847Zdgdldc9jI3VOZBY','IvQqybdyod','2018-04-21 14:44:11','2018-04-21 14:44:11'),(29,'Conner Hamill DDS','arden07@example.com','$2y$10$MSYdloutVf6dmQfiytF9buMV4','SFPlvnVzhe','2018-04-21 14:44:11','2018-04-21 14:44:11'),(30,'Skylar Stroman I','meaghan87@example.com','$2y$10$qWcYmA5Y1D2s/GHZv8Pv1evES','VJbG1uvLwM','2018-04-21 14:44:11','2018-04-21 14:44:11'),(31,'Prof. Adolphus O\'Conner IV','lonie17@example.org','$2y$10$mNH15ly6/HzeeKTq.IYXxeYX5','IPxNGgaht1','2018-04-21 14:44:11','2018-04-21 14:44:11'),(32,'Prof. Gideon Conroy','zwalsh@example.com','$2y$10$SnB9uwrHs1d/kxiRsZOaye4pE','IyAV2KwSEh','2018-04-21 14:44:11','2018-04-21 14:44:11'),(33,'Prof. Frankie Littel','lavada.eichmann@example.org','$2y$10$H7I.MAScarqCJ9VgRG0bWOy4V','99h8juM0lo','2018-04-21 14:44:11','2018-04-21 14:44:11'),(34,'Eda Flatley','windler.benedict@example.com','$2y$10$NM8JvAMAxQM9Y4h5dZAuyuQI.','yvE8EXzkSS','2018-04-21 14:44:11','2018-04-21 14:44:11'),(35,'Bettie Goodwin','abdullah86@example.org','$2y$10$ZLdXyDCkUBcj4s4tKrWbmuSIh','qzog8HQgbl','2018-04-21 14:44:11','2018-04-21 14:44:11'),(36,'Marcelo Dickens','alicia.kling@example.net','$2y$10$EdCz/WhJ1bE90TbuagFQPOeUf','z5smv2qsQx','2018-04-21 14:44:11','2018-04-21 14:44:11'),(37,'Amara Weimann','lempi72@example.com','$2y$10$oJyF7bZRLNw7rnVld4xbde.rb','NNYveECOBw','2018-04-21 14:44:11','2018-04-21 14:44:11'),(38,'Rylee Hermiston','hauck.dennis@example.com','$2y$10$Ri/VoWyOV1mEUjA6vPaoUe.LV','Y1rvJBflf9','2018-04-21 14:44:11','2018-04-21 14:44:11'),(39,'Prof. Brandi Runolfsson II','cheyanne56@example.net','$2y$10$P5dpaJsE8F/lNkzLHe/YTemo.','vipHQT2HmW','2018-04-21 14:44:12','2018-04-21 14:44:12'),(40,'Mr. Kamren Ward II','ben.smitham@example.org','$2y$10$moSSdmf8SaULtv307WXBn.9Ej','L1UXStOjBo','2018-04-21 14:44:12','2018-04-21 14:44:12'),(41,'Conrad Beer','jaylon16@example.net','$2y$10$jLDssyGE0tFedLb0NAa1GOXkj','OvR1mQpwj1','2018-04-21 14:44:12','2018-04-21 14:44:12'),(42,'Alysa Turner','cecile01@example.com','$2y$10$LAyS1d0unSpbhqoXL6hdJe2pI','D1o7x5zTUn','2018-04-21 14:44:12','2018-04-21 14:44:12'),(43,'Bailee Durgan PhD','ona64@example.net','$2y$10$4q2n7l7AnpGStqJefdGF8ep2R','txgdmaQbaN','2018-04-21 14:44:12','2018-04-21 14:44:12'),(44,'Prof. Kale Blick MD','diana80@example.com','$2y$10$dBe0nFlsUfXvs0HLY4r5zOTqZ','j3w1YVOmAa','2018-04-21 14:44:12','2018-04-21 14:44:12'),(45,'Jamal Gerhold','adams.dorothy@example.net','$2y$10$Ca8JITAtYw/v9GuOMGbD7Oe8h','yyUElpi1kx','2018-04-21 14:44:12','2018-04-21 14:44:12'),(46,'Keagan Rice','art.runte@example.org','$2y$10$bDflUSqD7oE7PwBi/i0BbO9IA','FEynMTUv5M','2018-04-21 14:44:12','2018-04-21 14:44:12'),(47,'Maurice Swaniawski','gilberto57@example.com','$2y$10$fqteG.onrWsR0qecrR/1Hee6G','UKfoYOPndh','2018-04-21 14:44:12','2018-04-21 14:44:12'),(48,'Mose McGlynn Jr.','vfriesen@example.org','$2y$10$DhHZiTVz2Gf9Ty7Q1Yh0seZWK','rpbkIZo4W6','2018-04-21 14:44:12','2018-04-21 14:44:12'),(49,'Kendra Powlowski II','jakayla80@example.com','$2y$10$dHHViz3kNj3JV5qhhsU1J.i8O','zRkFo17WM2','2018-04-21 14:44:12','2018-04-21 14:44:12'),(50,'Mr. Ulices Gulgowski V','yolanda.nader@example.com','$2y$10$kkD/C9WUW/qHIAAmauS30.CnN','Y6i930YLYP','2018-04-21 14:44:12','2018-04-21 14:44:12'),(51,'Julie Kutch III','melisa84@example.net','$2y$10$qFyt9TVNOywv/QMN8XrhnucLg','J5hVpgqMQS','2018-04-21 14:44:12','2018-04-21 14:44:12');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-21 16:46:16
