<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('create a new questionnaire');

//log in as your admin user
//This should be id of 1 if you created your manual login known user first
Auth::loginUsingId(1);

// Add db test data

// add a test user
$I->haveRecord('users', [
    'id' => '9999',
    'name' => 'testuser1',
    'email' => 'test1@user.com',
    'password' => 'password',
]);

// Add test questions
$I->haveRecord('questions', [
    'id' => '9900',
    'title' => 'question 1',
    'detail' => 'question1 detail',
]);
$I->haveRecord('questions', [
    'id' => '9901',
    'title' => 'question 2',
    'detail' => 'question2 detail',
]);


// add a test questionnaire to check that description can be seen in list at start

$I->haveRecord('questionnaires', [
    'id' => '9000',
    'title' => 'Questionnaire 1',
    'description' => 'questionnaire 1 description',
    'slug' => 'questionnaire1',
    'user_id' => 9999
]);


// add link data for questionnaire and question for the test entry

//swap to new user so tests are created at this users id.
Auth::logout();
Auth::loginUsingId(9999);

$I->haveRecord('question_questionnaire', [
    'questionnaire_id' => '9000',
    'question_id' => '9900',
]);




// tests /////////////////////////////////////////////

// create an questionnaire linked to one question
// When
$I->amOnPage('/admin/questionnaires');
$I->see('Questionnaires', 'h1');
$I->see('questionnaire 1');
$I->dontSee('questionnaire 2');
// And
$I->click('Add Questionnaire');

// Then
$I->amOnPage('/admin/questionnaires/create');
// And
$I->see('Add Questionnaire', 'h1');

$I->submitForm('#createquestionnaire', [
    'title' => 'Questionnaire 2',
    'description' => 'questionnaire 2 description',
    'slug' => 'questionnaire2',
    'question' => '9900',
]);

// how to handle the link table checking.
// check that the questionnaire has been written to the db then grab the new id ready to use as input to the link table.
// We don't have to search for the question id as we set that above and so we already know what it should have been set to.
$questionnaire = $I->grabRecord('questionnaires', ['title' => 'Questionnaire 2']);
$I->seeRecord('question_questionnaire', ['questionnaire_id' => $questionnaire->id, 'question_id' => '9900']);

// Then
$I->seeCurrentUrlEquals('/admin/questionnaires');
$I->see('Questionnaires', 'h1');
$I->see('Questionnaire 1');
$I->see('Questionnaire 2');

$I->click('Questionnaire 2'); // the title is a link to the detail page

// Check that the url has a similar path to this.. the last part is a regular expression to allow for a digit or more to be returned as an id.
//$I->seeCurrentUrlMatches('~/admin/Questionnaire/(\d+)~');
$I->see('Questionnaire 2', 'h1');
$I->see('questionnaire 2 description');
$I->see('creator: testuser1'); // Got to here in Passing first BBd test 4 errors on line 24
$I->see('questions:');
$I->see('question 1');

// create an questionnaire linked to two questions
// When
$I->amOnPage('/admin/questionnaires');
$I->see('Questionnaires', 'h1');
$I->see('Questionnaire 1');
$I->dontSee('Questionnaire 3');
// And
$I->click('Add Questionnaire');

// Then
$I->amOnPage('/admin/questionnaires/create');
// And
$I->see('Add Questionnaire', 'h1');
$I->submitForm('#createquestionnaire', [
    'title' => 'Questionnaire 3',
    'description' => 'questionnaire 3 description',
    'slug' => 'questionnaire3',
    'user' => 'testuser1',
    'question' => [9900,9901], // multi select drop down
]);

// how to handle the link table checking.

        /* On your controller

$questions = question::lists('name', 'id');

        On customer create view you can use.

        {{ Form::label('questions', 'questions') }}
        {{ Form::select('questions[]', $questions, null, ['id' => 'questions', 'multiple' => 'multiple']) }}

        Third parameter accepts a list of array as well. If you define a relationship on your model you can do this:

        {!! Form::label('questions', 'questions') !!}
        {!! Form::select('questions[]', $questions, $questionnaire->questions->lists('id')->all(), ['id' => 'questions', 'multiple' => 'multiple']) !!}

        */

// check that the questionnaire has been written to the db then grab the new id ready to use as input to the link table.
// We don't have to search for the question id as we set that above and so we already know what it should have been set to.
$questionnaire = $I->grabRecord('questionnaires', ['title' => 'Questionnaire 3']);
$I->seeRecord('questionnaire_question', ['questionnaire_id' => $questionnaire->id, 'question_id' => '9900']);
$I->seeRecord('questionnaire_question', ['questionnaire_id' => $questionnaire->id, 'question_id' => '9901']);

// Then
$I->seeCurrentUrlEquals('/admin/questionnaires');
$I->see('Questionnaires', 'h1');
$I->see('Questionnaire 1');
$I->see('Questionnaire 2');
$I->see('Questionnaire 3');

$I->click('Questionnaire 3'); // the title is a link to the detail page


// Check that the url has a similar path to this.. the last part is a regular expression to allow for a digit or more to be returned as an id.
//$I->seeCurrentUrlMatches('~$/admin/Questionnaire/(\d+)~');
$I->see('Questionnaire 3', 'h1');
$I->see('questionnaire 3 description');
$I->see('creator: testuser1');
$I->see('questions:');
$I->see('question 1');
$I->see('question 2');
