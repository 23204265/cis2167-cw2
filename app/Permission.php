<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
/**
*creates relations between permission and role models
*/
{
    public function roles()
    {
      return $this->belongsToMany(Role::class);
        }
}
