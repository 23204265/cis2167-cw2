<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
  protected $fillable = [
    'title',
    'detail',


  ];

  public function questionnaires()
  {
    return $this->belongsToMany('App\Questionnaire');
  }

  public function questions()
  {
    return $this->belongsToMany('App\Question');
  }
  public function users()
  {
    return $this->belongsToMany('App\User');
  }
}
