<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model

/**
*creates relations between permission and role models
*/
{
    public function permissions()
     {
      return $this->belongsToMany(Permission::class);
    }

    /*
    * allows the role to take a permission and add to the role
    */
    public function givePermissionTo(Permission $permission) {
      return $this->permission()->sync($permission);
    }
}
