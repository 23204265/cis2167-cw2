<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
  protected $fillable = [
    'title',
    'description',
    'slug',


  ];

  /**
  *Get the questions associated with the given questionnaire
  *
  * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
  */
  public function questions()
  {
    return $this->belongsToMany('App\Question');

  }
  /**
  *Get the user associated with the given questionnaire*
  *
  *@return mixed
  */

  public function user()
  {
    return $this->belongsTo('App\User', 'user_id');
  }
}
