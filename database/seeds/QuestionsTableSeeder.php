<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      {
          DB::table('questions')->insert([
              ['id' => 1, 'title' => "What is your favourite colour?", 'detail' => "select colour"],
              ['id' => 2, 'title' => "what is your favourite animal?", 'detail' => "choose animal"],
            ]);
      }
    }
}
