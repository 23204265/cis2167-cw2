<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('permissions')->insert([
        ['id' => 1,
            'name' => "see_adminnav",
            'label' => 'Can see the admin nav',

        ],
        ['id' => 2,
            'name' => "update_role",
            'label' => 'Can update a users roles',
        ],
        ['id' => 3,
            'name' => "create_role",
            'label' => 'Can create a new role',
          ],
          ['id' => 4,
              'name' => "see_all_users",
              'label' => 'Can see users page',
            ],
      ]);

  }
}
