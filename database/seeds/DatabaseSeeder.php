<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Question;
use App\Questionnaire;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
       /**
       *Truncate tables code
       */

       //disable foreign key check for this connection before running seeders
      DB::statement('SET FOREIGN_KEY_CHECKS=0;');
       Model::unguard();

       //truncate before adding in data with ids that are statement
       Question::truncate();
       User::truncate();
       Questionnaire::truncate();

       Model::reguard();
       //re-enable foreign key check for this connection
       DB::statement('SET FOREIGN_KEY_CHECKS=1;');

       /*
       *run seed files so known data is created first
       */
       $this->call(QuestionsTableSeeder::class);
       $this->call(UserTableSeeder::class);
       $this->call(RoleTableSeeder::class);
       $this->call(PermissionTableSeeder::class);
       $this->call(Permission_RoleTableSeeder::class);
       $this->call(Role_UserTableSeeder::class);

       /*
       * run factories
       */

         factory(User::class, 50)->create();
         factory(Questionnaire::class, 5)->create();

     }
 }
