<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionnairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('questionnaires', function (Blueprint $table) {
        $table->increments('id')->unique();
        $table->string('title')->unique();
        $table->text('description');
        $table->string('slug');
        $table->integer('user_id')->unsigned()->default(0);
        //$table->foreign('user_id')-references('id')->on('users')->onDelete('cascade');
        //$table->foreign('question_id')-references('id')->on('questions')->onDelete('cascade');
        $table->timestamps();
        $table->timestamp('published_at')->index();
      });
            }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('questionnaires');
    }
}
