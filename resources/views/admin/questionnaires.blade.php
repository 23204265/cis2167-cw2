<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Questionnaires</title>
</head>
<body>
<h1>Questionnaires</h1>

<section>
    @if (isset ($questionnaires))
        <ul>
            @foreach ($questionnaires as $questionnaire)
                <li><a href="/admin/questionnaires/{{ $questionnaire->id}}" name="{{ $questionnaire->title }}">{{ $questionnaire->title }}</a></li>
                <ul>{{$questionnaire->description}}</ul>
            @endforeach
        </ul>
    @else
        <p> no questionnaires added yet </p>
    @endif
</section>
{{ Form::open(array('action' => 'QuestionnaireController@create', 'method' => 'get')) }}
    <div class="row">
        {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}
    </div>
{{ Form::close() }}
</body>
</html>
