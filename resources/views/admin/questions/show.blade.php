<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>{{ $question->title }}</title>
</head>
<body>
  <h1> {{ $question->title }} </h1>
  <p> {{ $question->detail }} </p>

  <p> questions:</p>

  @if (isset ($question->answers))

  <ul>
    @foreach ($question->answers as $answer)
      <li><p>{{ $answer->answer_option }}</p></li>
    @endforeach
  </ul>

  @else
    <p> no answerss linked yet </p>
   @endif
   {{ Form::open(array('action' => 'AnswerController@create', 'method' => 'get')) }}
       <div class="row">
           {!! Form::submit('Add answer', ['class' => 'button']) !!}
       </div>
   {{ Form::close() }}


   <p> Creator: {{ $questionnaire->user->name }}</p>
</body>
</html>
