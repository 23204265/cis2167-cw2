<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Questions</title>
</head>
<body>
<h1>Questions</h1>

<section>
    @if (isset ($questions))
        <ul>
            @foreach ($questions as $question)
                <li><a href="/admin/questions/{{$question->id}}" name="{{ $question->title }}">{{ $question->title }}</a></li>
                <ul>{{$question->description}}</ul>
            @endforeach
        </ul>
    @else
        <p> no questions added yet </p>
    @endif
</section>
{{ Form::open(array('action' => 'QuestionController@create', 'method' => 'get')) }}
    <div class="row">
        {!! Form::submit('Add Question', ['class' => 'button']) !!}
    </div>
{{ Form::close() }}
</body>
</html>
