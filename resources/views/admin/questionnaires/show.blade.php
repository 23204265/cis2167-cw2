<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>{{ $questionnaire->title }}</title>
</head>
<body>
  <h1> {{ $questionnaire->title }} </h1>
  <p> {{ $questionnaire->description }} </p>

  <p> questions:</p>

  @if (isset ($questionnaire->questions))

  <ul>
    @foreach ($questionnaire->questions as $question)
      <li><p>{{ $question->title }}</p></li>
      <li><p>{{ $question->detail }}</p></li>
    @endforeach
  </ul>

  @else
    <p> no questions linked yet </p>
   @endif
   {{ Form::open(array('action' => 'QuestionController@create', 'method' => 'get')) }}
       <div class="row">
           {!! Form::submit('Add Question', ['class' => 'button']) !!}
       </div>
   {{ Form::close() }}


   <p> Creator: {{ $questionnaire->user->name }}</p>
</body>
</html>
